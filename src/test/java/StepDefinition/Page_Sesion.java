package StepDefinition;

import static org.junit.Assert.assertEquals;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import init.Page_BasePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import utils.Constantes;

public class Page_Sesion extends Page_BasePage {
	
	private JavascriptExecutor js;
	
	private boolean highLigh(WebElement element) {
		js = (JavascriptExecutor) driver;
		for (int iCnt = 0; iCnt < 3; iCnt++) {
			try {
				js.executeScript("arguments[0].setAttribute('style', 'border:3px solid red;background: yellow')", element);
				Thread.sleep(1000);
			} catch (Exception e) {
				System.err.println("Javascript | highLigh | Excepction descrip:" + e.getMessage());
				return false;
			}
		}
		return true;
	}

	public void inicioNavegador() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(Constantes.URL);
	}

	public void categories() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement categoriesButton = driver
				.findElement(By.xpath("//div[@class='HamburgerIcon_hamburgerContain__1BOAJ']"));
		highLigh(categoriesButton);
		categoriesButton.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Tecnología')]")));

	}

	public void tecnologia() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement technology = driver.findElement(By.xpath("//div[contains(text(),'Tecnología')]"));
		technology.click();
		highLigh(technology);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Consolas")));

	}

	public void consola() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement consola = driver.findElement(By.linkText("Consolas"));
		highLigh(consola);
		consola.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Marca')]")));
	}

	public void marca(String brand) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement accordionBrand = driver.findElement(By.xpath("//div[contains(text(),'Marca')]"));
		new Actions(driver).moveToElement(accordionBrand).perform();
		highLigh(accordionBrand);
		//accordionBrand.click();
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("testId-Multiselect-Marca")));
		driver.findElement(By.xpath("//body/div[@class='site-wrapper']/main[@id='main']/div[@id='fbra_browseProductList']/div[@class='fb-filters']/div[@class='content-plp-app']/section[@id='fb-vertical-filters']/div[@class='fb-filter-bar']/div/div[@id='vertical-filters-custom']/div[1]/div[2]/div[1]/input[1]")).sendKeys(brand);
		WebElement nameBrand = driver.findElement(By.xpath("//div[@class='fb-filter-bar']//div[1]//div[2]//ul[1]//div[1]//li[1]//label[1]//span[1]"));
		highLigh(nameBrand);
		nameBrand.click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[@class='fb-form__label--selections']")));
	}

	public void verProducto() throws InterruptedException {
		int isPresent = driver
				.findElements(
						By.xpath("/html[1]/body[1]/div[2]/main[1]/div[3]/div[1]/div[1]/section[2]/div[2]/div[1]"))
				.size();
		System.out.println(isPresent);
		if (isPresent >= 1) {
			WebElement seeProduct = driver.findElement(By.xpath("/html[1]/body[1]/div[2]/main[1]/div[3]/div[1]/div[1]/section[2]/div[2]/div[1]/div[3]/a[1]/div[2]/span[1]/span[1]/span[1]"));
			new Actions(driver).moveToElement(seeProduct).perform();
			highLigh(seeProduct);		
			seeProduct.click();
			Thread.sleep(3000);
		} else {
			System.out.println("No hay productos");
		}
	}

	public void aumentarCantidad() {
		WebElement addProduct =	driver.findElement(By.xpath("//button[@class='fb-quantity-input__plus']"));
		new Actions(driver).moveToElement(addProduct).perform();
		highLigh(addProduct);
		addProduct.click();
		addProduct.click();
	}

	public void validarCantidad() {
		WebElement quantityInput = driver.findElement(By.name("quantity1"));
		highLigh(quantityInput);
		String actualValue = quantityInput.getAttribute("value");
		assertEquals("Validación cantidad", Constantes.expected, actualValue);

	}

	public WebDriver close() {
		driver.close();
		return driver;
	}

}
