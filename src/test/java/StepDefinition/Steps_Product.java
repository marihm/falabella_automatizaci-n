package StepDefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Steps_Product {

	
	Page_Sesion googleHomepage = new Page_Sesion();
	
	
	@Given("Inicialización de Navegador")
	public void inicializa_Navegador() throws Throwable {
		googleHomepage.inicioNavegador();
	}

	@When("Se hace clic en botón Categoría")
	public void hace_clic_en_botón_Categoría() {
		googleHomepage.categories();
	    
	}

	@And("Selecciona tecnología")
	public void selecciona_tecnología() {
		googleHomepage.tecnologia();
	}

	@And("Hace clic en consolas")
	public void hace_clic_en_consolas() {
		googleHomepage.consola();
	}

	@And("Selecciona marca \"([^\"]*)\"$")
	public void selecciona_marca(String brand) {
		googleHomepage.marca(brand);
	}

	@And("Selecciona ver producto")
	public void selecciona_ver_producto() throws InterruptedException {
		googleHomepage.verProducto();
	}

	@And("Aumenta cantidad de productos a \"([^\"]*)\"$")
	public void aumenta_cantidad_de_productos_a(String string) {
		googleHomepage.aumentarCantidad();
	}

	@Then("Valida cantidad de productos")
	public void valida_cantidad_de_productos() {
		googleHomepage.validarCantidad();
	}
	
	@And("Cierra Navegador")
	public void cierra_navegador() {
		googleHomepage.close();
	}
		

}
