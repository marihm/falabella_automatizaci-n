package runner;		

import org.junit.runner.RunWith;		
import cucumber.api.CucumberOptions;		
import cucumber.api.junit.Cucumber;		

@RunWith(Cucumber.class)				
@CucumberOptions(
		features="Features",
		glue={"StepDefinition"}, 
		plugin = { "pretty", "json:target/cucumber.json", "html:target/cucumber-html-report", "junit:target/cucumber.xml" },
		monochrome = true
)

public class RunnerTest 				
{		

}		