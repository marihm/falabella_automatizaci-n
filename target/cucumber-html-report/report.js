$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:Features/1Product.feature");
formatter.feature({
  "name": "Buscar consola de video.",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@test1"
    }
  ]
});
formatter.scenario({
  "name": "Como usuario de falabella quiero buscar una consola de video.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@test1"
    }
  ]
});
formatter.step({
  "name": "Inicialización de Navegador",
  "keyword": "Given "
});
formatter.match({
  "location": "Steps_Product.inicializa_Navegador()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Se hace clic en botón Categoría",
  "keyword": "When "
});
formatter.match({
  "location": "Steps_Product.hace_clic_en_botón_Categoría()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Selecciona tecnología",
  "keyword": "And "
});
formatter.match({
  "location": "Steps_Product.selecciona_tecnología()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Hace clic en consolas",
  "keyword": "And "
});
formatter.match({
  "location": "Steps_Product.hace_clic_en_consolas()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Selecciona marca \"ACTIVISION\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps_Product.selecciona_marca(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Selecciona ver producto",
  "keyword": "And "
});
formatter.match({
  "location": "Steps_Product.selecciona_ver_producto()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Aumenta cantidad de productos a \"3\"",
  "keyword": "And "
});
formatter.match({
  "location": "Steps_Product.aumenta_cantidad_de_productos_a(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Valida cantidad de productos",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps_Product.valida_cantidad_de_productos()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Cierra Navegador",
  "keyword": "And "
});
formatter.match({
  "location": "Steps_Product.cierra_navegador()"
});
formatter.result({
  "status": "passed"
});
});